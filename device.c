/*
 * Copyright (c) 2011 Martin Pieuchot
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/queue.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "plug.h"

int	 plug_is_block_device(const char *);

const char* plug_events[] = {
	NULL,
	"add",
	"remove",
};

struct device *
plug_device_new(const char *name, const char *subsystem)
{
	struct device	*dev;
	char		 devpath[24];

	dev = calloc(1, sizeof(*dev));
	if (dev == NULL)
		return (NULL);

	snprintf(devpath, sizeof(devpath), "%s/%s", DEVPATH, name);
	if (stat(devpath, &dev->sb) == 0) {
		dev->devnode = strndup(devpath, sizeof(devpath));
		if (dev->devnode == NULL) {
			free(dev);
			return (NULL);
		}
	}

	strlcpy(dev->pd_xname, name, sizeof(dev->pd_xname));
	TAILQ_INIT(&dev->properties);

	if (subsystem != NULL)
		dev->subsystem = subsystem;
	else if (name[0] == 'u') /* XXX Assumption */
		dev->subsystem = plug_subsystems[SUBSYS_USB];
	else if (plug_is_block_device(name))
		dev->subsystem = plug_subsystems[SUBSYS_BLOCK];
	else if (strncmp(name, "video", 5) == 0)
		dev->subsystem = plug_subsystems[SUBSYS_VIDEO];

	plug_device_ref(dev);
	if (plug_set_default_properties(dev)) {
		plug_device_unref(dev);
		return (NULL);
	}

	/* XXX Should error from subsystems properties be fatal? */
	if (dev->subsystem == plug_subsystems[SUBSYS_USB])
		plug_usb_set_default_properties(dev);

	if (dev->subsystem == plug_subsystems[SUBSYS_BLOCK])
		plug_block_set_default_properties(dev);

	if (dev->subsystem == plug_subsystems[SUBSYS_VIDEO])
		plug_video_set_default_properties(dev);

	return (dev);
}

void
plug_device_ref(struct device *dev)
{
	dev->ref++;
}

void
plug_device_unref(struct device *dev)
{
	dev->ref--;
	if (dev->ref == 0) {
		plug_remove_properties(dev);
		free(dev->devnode);
		free(dev);
	}
}

struct device *
plug_device_create_from_devnum(dev_t devnum, char t)
{
	mode_t	 type;
	char	*name;

	if (t == 'b')
		type = S_IFBLK;
	else if (t == 'c')
		type = S_IFCHR;
	else
		return (NULL);

	name = devname(devnum, type);
	if (strncmp(name, "??", 2) == 0)
		return (NULL);

	return (plug_device_new(name, NULL));
}

int
plug_is_block_device(const char *name)
{
	int len;

	len = strlen(name);
	if (len < 3)
		return (0);

	if ((name[0] == 'c' ||
	     name[0] == 'f' ||
	     name[0] == 'r' ||
	     name[0] == 's' ||
	     name[0] == 'w') && name[1] == 'd')
		return (1);

	if (len < 4)
		return (0);

	if (name[0] == 'c' && name[1] == 'c' && name[2] == 'd')
		return (1);

	if (name[0] == 'v' && name[1] == 'n' && name[2] == 'd')
		return (1);

	if (len < 5)
		return (0);

	if (name[0] == 'r' && name[1] == 'a' && name[2] == 'i' && name[3] =='d')
		return (1);

	return (0);
}
