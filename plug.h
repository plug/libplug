/*
 * Copyright (c) 2011 Martin Pieuchot
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _PLUG_H_
#define _PLUG_H_

#include <sys/queue.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <stdarg.h>

#define DEVPATH		"/dev"

#define EVENT_NONE	0
#define EVENT_ADD	1	/* HOTPLUG_DEVAT */
#define EVENT_REMOVE	2	/* HOTPLUG_DEVDT */

extern const char *plug_events[];

#define SUBSYS_BLOCK	0
#define SUBSYS_MEM	1
#define SUBSYS_NET	2
#define SUBSYS_TTY	3
#define SUBSYS_USB	4
#define SUBSYS_VIDEO	5
#define SUBSYS_ALL	6

extern const char *plug_subsystems[SUBSYS_ALL];	/* known subsystems */

/* Not used.
 *
 * libudev uses a context struct to store informations shared with the
 * udev system. This structure is define only for source compatibility.
 */
struct plug;

struct monitor {
	int			devfd;		/* hotplug device */
};

struct property {
	TAILQ_ENTRY(property)	 list;
	char			 key[32];
	char			 val[32];
};

struct device {
	TAILQ_ENTRY(device)	 list;
	const char		*subsystem;	/* linux-like subsystem	   */
	char			*devnode;	/* /dev/node[0-9]	   */
	char			 pd_xname[16];	/* used for syspath compat */
	int			 devclass;
	int			 flags;
	int			 ref;

	struct stat		 sb;

	struct device 		*parent;
	TAILQ_HEAD(, property)	 properties;
};

TAILQ_HEAD(devlist, device);


/* device.c */
void		 plug_device_ref(struct device *);
void		 plug_device_unref(struct device *);
struct device 	*plug_device_new(const char *, const char *);
struct device	*plug_device_create_from_devnum(dev_t devnum, char t);

/* monitor.c */
struct monitor	*plug_monitor_new(void);
void		 plug_monitor_free(struct monitor *);
int		 plug_monitor_open_fd(struct monitor*);
struct device	*plug_monitor_read_event(struct monitor *);

/* property.c */
struct property	*plug_get_property(struct device *, const char*);
int		 plug_set_property(struct device *, const char*, const char*);
void		 plug_remove_properties(struct device *);
int		 plug_remove_property(struct device *, const char* );
int		 plug_set_default_properties(struct device *);

/* list.c */
struct devlist	*plug_devlist_new(void);
void		 plug_devlist_free(struct devlist *);
struct property	*plug_devlist_get_devpaths(struct devlist *);
int		 plug_devlist_scan(struct devlist *, const char *);
int		 plug_devlist_scan_except(struct devlist *, const char *);
int		 plug_devlist_scan_all(struct devlist *);

/* subsystems.c */
int		 plug_usb_scan(struct devlist *);
struct device	*plug_usb_read_node(int, int);
void		 plug_usb_set_default_properties(struct device *);
void		 plug_block_set_default_properties(struct device *);
int		 plug_video_scan(struct devlist *);
void		 plug_video_set_default_properties(struct device *);

#endif /* _PLUG_H_ */
