/*
 * Copyright (c) 2011 Martin Pieuchot
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>
/* XXX Don't include <sys/device.h> to not create a conflict
 * with our "struct device" type.
 *
 * Later hotplug monitoring should be done using the imsg framework because
 * we don't want every application using libplug to be run as root in order
 * to read /dev/hotplug.
 */
enum devclass {
	DV_DULL,		/* generic, no special info */
	DV_CPU,			/* CPU (carries resource utilization) */
	DV_DISK,		/* disk drive (label, etc) */
	DV_IFNET,		/* network interface */
	DV_TAPE,		/* tape device */
	DV_TTY			/* serial line interface (???) */
};
#include <sys/stat.h>
#include <sys/hotplug.h>


#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "plug.h"

#define HOTPLUGDEV "/dev/hotplug"

struct monitor *
plug_monitor_new(void)
{
	struct monitor	*m;

	m = calloc(1, sizeof(*m));
	return (m);
}

void
plug_monitor_free(struct monitor *m)
{
	if (m->devfd >= 0)
		close(m->devfd);
	free(m);
}

int
plug_monitor_open_fd(struct monitor* mon)
{
	if ((mon->devfd = open(HOTPLUGDEV, O_RDONLY)) == -1) {
		warn("%s", HOTPLUGDEV);
		return (-1);
	}

	return (0);
}

struct device *
plug_monitor_read_event(struct monitor *mon)
{
	struct hotplug_event	 he;
	struct device		*dev;
	const char 		*subsystem = NULL;
	char			 devname[16];

retry:
	if (read(mon->devfd, &he, sizeof(he)) == -1) {
		if (errno == EINTR)
			goto retry;
		warn("read error");
		return (NULL);
	}

	switch (he.he_devclass) {
	case DV_DISK:
		snprintf(devname, sizeof(devname), "%s%c", he.he_devname, 'c');
		subsystem = plug_subsystems[SUBSYS_BLOCK];
		break;
	case DV_DULL:
		if (strncmp(he.he_devname, "ugen", 4) == 0) {
			snprintf(devname, sizeof(devname),
				 "%s.00", he.he_devname);
			subsystem = plug_subsystems[SUBSYS_USB];
			break;
		}
	default:
		warnx("device \"%s\" not supported.\n", he.he_devname);
		return (NULL);
	}

	dev = plug_device_new(devname, subsystem);

	if (dev != NULL) {
		dev->flags = he.he_type;
		dev->devclass = he.he_devclass;
	}

	return (dev);
}
