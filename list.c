/*
 * Copyright (c) 2011 Martin Pieuchot
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/param.h>
#include <sys/queue.h>

#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "plug.h"

struct devlist *
plug_devlist_new(void)
{
	struct devlist *list;

	list = calloc(1, sizeof(*list));
	if (list == NULL)
		return (NULL);

	TAILQ_INIT(list);

	return (list);
}

void
plug_devlist_free(struct devlist * devlist)
{
	struct device	*dev;

	TAILQ_FOREACH(dev, devlist, list) {
		TAILQ_REMOVE(devlist, dev, list);
		plug_device_unref(dev);
	}
	free(devlist);
}

struct property *
plug_devlist_get_devpaths(struct devlist *devlist)
{
	struct device	*dev, *fakedev;

	if (TAILQ_EMPTY(devlist))
		return (NULL);
	/*
	 * XXX Here it is a hack. Because we don't use a special type
	 * for udev's entry_list we create a list with the devname
	 * (udev's syspath) of all the devices in the list.
	 */
	fakedev = calloc(1, sizeof(*fakedev));
	if (fakedev == NULL)
		return (NULL);

	strlcpy(fakedev->pd_xname, "null", sizeof(fakedev->pd_xname));
	plug_device_ref(fakedev);
	TAILQ_INIT(&fakedev->properties);

	/* XXX libudev sort the list by devpath. */
	TAILQ_FOREACH(dev, devlist, list) {
		plug_set_property(fakedev, dev->pd_xname, "");
	}

	TAILQ_INSERT_HEAD(devlist, fakedev, list);

	return TAILQ_FIRST(&fakedev->properties);
}

int
plug_devlist_scan(struct devlist *list, const char *name)
{
	if (name == NULL)
		return (-1);

	if (strcmp(name, "usb") == 0)
		return (plug_usb_scan(list));

	if (strcmp(name, "video4linux") == 0)
		return (plug_video_scan(list));

	warnx("subsystem \"%s\" not supported.", name);

	return (0);
}

int
plug_devlist_scan_except(struct devlist *list, const char *name)
{
	unsigned 	i;

	for (i = 0; i < nitems(plug_subsystems); i++) {
		if (strcmp(plug_subsystems[i], name) != 0)
			plug_devlist_scan(list, plug_subsystems[i]);
	}

	return (0);
}

int
plug_devlist_scan_all(struct devlist *list)
{
	unsigned	i;

	for (i = 0; i < nitems(plug_subsystems); i++) {
		plug_devlist_scan(list, plug_subsystems[i]);
	}

	return (0);
}
