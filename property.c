/*
 * Copyright (c) 2011 Martin Pieuchot
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/queue.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "plug.h"

int
plug_set_property(struct device *dev, const char* key, const char* val)
{
	struct property *p;

	p = calloc(1, sizeof(*p));
	if (p == NULL)
		return (1);

	strlcpy(p->key, key, sizeof(p->key));
	strlcpy(p->val, val, sizeof(p->val));

	TAILQ_INSERT_TAIL(&dev->properties, p, list);

	return (0);
}

struct property *
plug_get_property(struct device *dev, const char* key)
{
	struct property	*p;

	TAILQ_FOREACH(p, &dev->properties, list) {
		if (strcmp(p->key, key) == 0)
			return (p);
	}

	return (NULL);
}

void
plug_remove_properties(struct device *dev)
{
	struct property	*p, *p0;

	TAILQ_FOREACH_SAFE(p, &dev->properties, list, p0) {
		TAILQ_REMOVE(&dev->properties, p, list);
		free(p);
	}
}

int
plug_remove_property(struct device *dev, const char* key)
{
	struct property	*p;

	TAILQ_FOREACH(p, &dev->properties, list) {
		if (strncmp(p->key, key, sizeof(p->key) - 1) == 0) {
			TAILQ_REMOVE(&dev->properties, p, list);
			free(p);
			return (0);
		}
	}

	return (1);
}

int
plug_set_default_properties(struct device *dev)
{
	char nb[5];

	/*
	 * First property should always be DEVNAME because it can be
	 * used to recreate the device from:
	 *	udev_device_new_from_syspath(context, devname)
	 *
	 * For us syspath is a alias for devpath.
	 */
	if (plug_set_property(dev, "DEVNAME", dev->pd_xname))
		return (1);

	if (dev->subsystem)
		if (plug_set_property(dev, "SUBSYSTEM", dev->subsystem))
			return (1);

	if (dev->sb.st_rdev) {
		snprintf(nb, sizeof(nb), "%u", major(dev->sb.st_rdev));
		if (plug_set_property(dev, "MAJOR", nb))
			return (1);

		snprintf(nb, sizeof(nb), "%u", minor(dev->sb.st_rdev));
		if (plug_set_property(dev, "MINOR", nb))
			return (1);
	}

	if (dev->devnode)
		if (plug_set_property(dev, "DEVNODE", dev->devnode))
			return (1);

	if (dev->sb.st_mode) {
		snprintf(nb, sizeof(nb), "%04o", dev->sb.st_mode & 0777);
		if (plug_set_property(dev, "DEVMODE", nb))
			return (1);
	}

	return (0);
}

