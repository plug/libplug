#

LIBDIR=	/usr/local/lib
INCDIR= /usr/local/include
MANDIR=	/usr/local/man/cat

LIB=	plug
DEBUG=	-g3

HDRS=	plug.h
SRCS=	device.c monitor.c list.c property.c subsystems.c

HDRS+=	libudev.h
SRCS+=	libudev.c

WARNINGS=Yes

CDIAGFLAGS+= -Wno-long-long -W -Wshadow -Wnested-externs -Wformat=2 -Winline 
CDIAGFLAGS+= -Wmissing-prototypes -Wstrict-prototypes -Wmissing-declarations
CDIAGFLAGS+= -Wwrite-strings -Wpointer-arith -Wundef -Wbad-function-cast
CDIAGFLAGS+= -Wcast-align -Wsign-compare -Wchar-subscripts

MAN=	plug.3

MLINKS=	plug.3 libudev.3

afterinstall: includes

includes:
	@cd ${.CURDIR}; for i in ${HDRS}; do \
	    j="cmp -s $$i ${DESTDIR}${INCDIR}/$$i || \
		${INSTALL} ${INSTALL_COPY} -o ${BINOWN} -g ${BINGRP} \
		-m 444 $$i ${DESTDIR}${INCDIR}"; \
	    echo $$j; \
	    eval "$$j"; \
	done

.include <bsd.lib.mk>
