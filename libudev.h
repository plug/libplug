/*
 * Copyright (c) 2011 Martin Pieuchot
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef _LIBUDEV_H_
#define _LIBUDEV_H_

/* Map udev types to libplug ones */
#define	udev			plug
#define udev_device		device
#define udev_list_entry		property
#define udev_enumerate		devlist
#define udev_monitor		monitor

#include "plug.h"

/*
 * Context
 */
void		 udev_unref(struct plug *);
struct plug	*udev_new(void);
const char 	*udev_get_sys_path(struct plug *);
const char 	*udev_get_dev_path(struct plug *);


/*
 * Device
 */
struct device	*udev_device_ref(struct device *);
void		 udev_device_unref(struct device *);

struct property	*udev_device_get_devlinks_list_entry(struct device *);
int		 udev_device_get_is_initialized(struct device *);
const char 	*udev_device_get_devnode(struct device *);
dev_t 		 udev_device_get_devnum(struct device *);
const char 	*udev_device_get_devtype(struct device *);
const char 	*udev_device_get_devpath(struct device *);
const char 	*udev_device_get_driver(struct device *);
const char	*udev_device_get_sysname(struct device *);
const char	*udev_device_get_sysnum(struct device *);
const char	*udev_device_get_syspath(struct device *);
const char	*udev_device_get_sysattr_value(struct device *, const char *);
const char	*udev_device_get_action(struct device *);
const char	*udev_device_get_subsystem(struct device *);
const char 	*udev_device_get_property_value(struct device *, const char *);
struct device 	*udev_device_get_parent(struct device *);
struct device 	*udev_device_get_parent_with_subsystem_devtype(struct device *,
    const char *, const char *);

struct device	*udev_device_new_from_devnum(struct plug*, char, dev_t);
struct device	*udev_device_new_from_syspath(struct plug*, const char *);
struct device	*udev_device_new_from_subsystem_sysname(struct plug*, const char *, const char *);

unsigned long long udev_device_get_seqnum(struct device *);
unsigned long long udev_device_get_usec_since_initialized(struct device *);

struct property *udev_device_get_properties_list_entry(struct device *);
struct property	*udev_device_get_tags_list_entry(struct device *);


/*
 * Enum
 */
struct devlist	*udev_enumerate_new(struct plug*);
void	 	 udev_enumerate_unref(struct devlist *);

struct property	*udev_enumerate_get_list_entry(struct devlist *);
struct plug	*udev_enumerate_get_udev(struct devlist *);
int		 udev_enumerate_scan_devices(struct devlist *);
int		 udev_enumerate_scan_subsystems(struct devlist *);

int		 udev_enumerate_add_match_tag(struct devlist *, const char *);
int		 udev_enumerate_add_syspath(struct devlist *, const char *);
int		 udev_enumerate_add_match_is_initialized(struct devlist *);
int		 udev_enumerate_add_match_property(struct devlist *, const char *, const char *);
int		 udev_enumerate_add_match_subsystem(struct devlist *, const char *);
int 		 udev_enumerate_add_nomatch_subsystem(struct devlist *, const char *);
int		 udev_enumerate_add_match_sysattr(struct devlist *, const char *, const char *);
int		 udev_enumerate_add_nomatch_sysattr(struct devlist *, const char *, const char *);
int		 udev_enumerate_add_match_sysname(struct devlist *, const char *);


/*
 * List
 */
const char	*udev_list_entry_get_name(struct property *);
const char	*udev_list_entry_get_value(struct property *);
struct property	*udev_list_entry_get_next(struct property *);

#define 	 udev_list_entry_foreach(entry, first)			\
			for ((entry) = (first);				\
			     (entry) != NULL;				\
			     (entry) = udev_list_entry_get_next(entry))


/*
 * Monitor
 */
struct monitor	*udev_monitor_new_from_netlink(struct plug*, const char *);
void		 udev_monitor_unref(struct monitor *);
int		 udev_monitor_get_fd(struct monitor *);
int 		 udev_monitor_filter_add_match_subsystem_devtype(struct monitor *, const char *, const char *);
struct device	*udev_monitor_receive_device(struct monitor *);
int		 udev_monitor_enable_receiving(struct monitor *);


#endif /* _LIBUDEV_H_ */
