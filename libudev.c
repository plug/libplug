/*
 * Copyright (c) 2011 Martin Pieuchot
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <err.h>

#include "libudev.h"


/**
 * udev_new:
 * @context: (not used)
 *
 * Returns: Always returns #NULL.
 **/
struct plug *
udev_new(void)
{
	return (NULL);
}


/**
 * udev_unref:
 * @context: (not used)
 **/
void
udev_unref(struct plug *context)
{
	/* Nothing to do. */
}


/**
 * udev_get_sys_path:
 * @context: (not used)
 *
 * Sys is no supported, retrieve the device directory path.
 *
 * Returns: the device directory path
 **/
const char *
udev_get_sys_path(struct plug *context)
{
	return (DEVPATH);
}


/**
 * udev_get_dev_path:
 * @context: (not used)
 *
 * Retrieve the device directory path, always returns "/dev".
 *
 * Returns: the device directory path
 **/
const char *
udev_get_dev_path(struct plug *context)
{
	return (DEVPATH);
}

/**
 * udev_device_ref:
 * @dev: plug device
 *
 * Increase the reference count of a device.
 *
 * Returns: the passed device
 **/
struct device *
udev_device_ref(struct device *dev)
{
	plug_device_ref(dev);
	return (dev);
}

/**
 * udev_device_unref:
 * @dev: plug device
 *
 * Decrease the reference count of a device.
 * If the counter reaches zero, the device's resources are released.
 **/
void
udev_device_unref(struct device *dev)
{
	plug_device_unref(dev);
}


/**
 *
 **/
struct device *
udev_device_new_from_subsystem_sysname(struct plug* context, const char *ssys,
    const char *name)
{
	warnx("function \"%s\" not supported.", __func__);
	return (NULL);
}


/**
 * udev_device_get_devlinks_list_entry:
 * @dev: plug device
 *
 * Links are not supported, always returns #NULL.
 *
 * Returns: #NULL
 **/
struct property *
udev_device_get_devlinks_list_entry(struct device *dev)
{
	return (NULL);
}


/**
 * udev_device_get_sysname:
 * @dev: plug device
 *
 * Sys is no supported, retrieve the device name.
 *
 * Returns: the dev name of the device dev
 **/
const char *
udev_device_get_sysname(struct device *dev)
{
	return (dev->pd_xname);
}

const char *
udev_device_get_syspath(struct device *dev)
{
	return (udev_device_get_sysname(dev));
}

/**
 * udev_device_get_action:
 * @dev: plug device
 *
 * Supported actions are: add, remove.
 *
 * Returns: the action string or #NULL if the device was not received
 * through a monitor.
 **/
const char *
udev_device_get_action(struct device *dev)
{
	if (dev->flags)
		return (plug_events[dev->flags]);
	return (NULL);
}


/**
 * udev_device_get_is_initialized:
 * @dev: plug device
 *
 * Returns: 1 if the device has a device node, 0 otherwise.
 */
int
udev_device_get_is_initialized(struct device *dev)
{
	if (dev->devnode != NULL)
		return (1);
	return (0);
}


/**
 * udev_device_get_devnode:
 * @dev: plug device
 *
 * Returns: the device node absolute path if it exists, #NULL otherwise.
 **/
const char *
udev_device_get_devnode(struct device *dev)
{
	return (dev->devnode);
}


/**
 * udev_device_get_devnum:
 * @dev: plug device
 *
 * Returns: the device major and minor numbers.
 **/
dev_t
udev_device_get_devnum(struct device *dev)
{
	return (dev->sb.st_rdev);
}

const char *
udev_device_get_devtype(struct device *dev)
{
	warnx("function \"%s\" not supported.", __func__);
	return (NULL);
}


/**
 * udev_device_get_devpath:
 * @dev: plug device
 *
 * Returns: the device node absolute path if it exists, #NULL otherwise.
 **/
const char *
udev_device_get_devpath(struct device *dev)
{
	return (udev_device_get_devnode(dev));
}

const char *
udev_device_get_driver(struct device *dev)
{
	/* Can be exported from cfdata struct in kernel space? */
	warnx("function \"%s\" not supported.", __func__);
	return (NULL);
}

/**
 * udev_device_get_subsystem:
 * @dev: plug device
 *
 * Retrieve an udev-compatible subsystem string.
 *
 * Returns: the subsystem name of the device, or #NULL if it is not set.
 **/
const char *
udev_device_get_subsystem(struct device *dev)
{
	return (dev->subsystem);
}


/**
 * udev_device_get_sysattr_value:
 * @dev: (not used)
 * @attr: (not used)
 *
 * Sys is not supported, always returns #NULL.
 *
 * Returns: #NULL
 **/
const char *udev_device_get_sysattr_value(struct device *dev, const char *attr)
{
	return (NULL);
}


/**
 * udev_device_get_sysnum:
 * @dev: plug device
 *
 * Sys is not supported, always returns #NULL.
 *
 * Returns: #NULL
 **/
const char *
udev_device_get_sysnum(struct device *dev)
{
	return (NULL);
}


/**
 * udev_device_get_parent:
 * @dev: plug device
 *
 * TODO
 *
 * Returns:  #NULL.
 **/
struct device *
udev_device_get_parent(struct device *dev)
{
	return (dev->parent);
}


/**
 *
 **/
struct device *
udev_device_get_parent_with_subsystem_devtype(struct device *dev,
    const char *subsystem, const char *devtype)
{
	warnx("function \"%s\" not supported.", __func__);
	return (NULL);
}


/**
 *
 **/
unsigned long long
udev_device_get_seqnum(struct device *dev)
{
	warnx("function \"%s\" not supported.", __func__);
	return (0);
}


/**
 *
 **/
unsigned long long
udev_device_get_usec_since_initialized(struct device *dev)
{
	warnx("function \"%s\" not supported.", __func__);
	return (0);
}


/**
 *
 **/
struct property *
udev_device_get_tags_list_entry(struct device *dev)
{
	warnx("function \"%s\" not supported.", __func__);
	return (NULL);
}

struct device *
udev_device_new_from_devnum(struct plug* context, char t, dev_t devnum)
{
	return (plug_device_create_from_devnum(devnum, t));
}

struct device *
udev_device_new_from_syspath(struct plug* context, const char *name)
{
	return (plug_device_new(name, NULL));
}

const char *
udev_device_get_property_value(struct device *dev, const char *key)
{
	struct property	*p;

	p = plug_get_property(dev, key);

	if (p)
		return (p->val);

	return (NULL);
}

struct property *
udev_device_get_properties_list_entry(struct device *dev)
{
	return TAILQ_FIRST(&dev->properties);
}

/**
 *
 **/
const char *
udev_list_entry_get_name(struct property *p)
{
	return (p->key);
}


/**
 *
 **/
const char *
udev_list_entry_get_value(struct property *p)
{
	return (p->val);
}


/**
 *
 **/
struct property *
udev_list_entry_get_next(struct property *p)
{
	return TAILQ_NEXT(p, list);
}


/**
 * udev_monitor_new_from_netlink:
 * @context: (not used)
 * @name: (not used)
 *
 * Create new monitor.
 */
struct monitor *
udev_monitor_new_from_netlink(struct plug* hotplug, const char *name)
{
	return (plug_monitor_new());
}


/**
 * udev_monitor_unref:
 * @mon: plug monitor
 *
 * Close the bound socket and free monitor's ressources.
 *
 **/
void
udev_monitor_unref(struct monitor *mon)
{
	plug_monitor_free(mon);
}

/**
 * udev_monitor_get_fd:
 * @mon: plug monitor
 *
 * Returns: the file descriptor to read device events
 */
int
udev_monitor_get_fd(struct monitor *mon)
{
	return (mon->devfd);
}

/**
 * udev_monitor_filter_add_match_subsystem_devtype:
 * @mon: plug monitor
 * @subsystem: name of a subsystem
 * @devtype:
 *
 * TODO
 *
 * Returns: 0
 */
int
udev_monitor_filter_add_match_subsystem_devtype(struct monitor *mon,
    const char *subsystem, const char *devtype)
{
	return (0);
}

/**
 * udev_monitor_filter_add_match_subsystem_devtype:
 * @mon: plug monitor
 *
 * Returns: 0 on success, a negative value otherwise.
 */
int
udev_monitor_enable_receiving(struct monitor *mon)
{
	return (plug_monitor_open_fd(mon));
}


/**
 *
 **/
struct device *
udev_monitor_receive_device(struct monitor *mon)
{
	return (plug_monitor_read_event(mon));
}


/**
 * udev_enumerate_new:
 * @context: (not used)
 *
 * Returns: an empty device list.
 **/
struct devlist *
udev_enumerate_new(struct plug* context)
{
	return (plug_devlist_new());
}


/**
 * udev_enumerate_unref:
 * @list: plug device list
 *
 * Releases all resources of the device list.
 **/
void
udev_enumerate_unref(struct devlist *list)
{
	plug_devlist_free(list);
}


/**
 * udev_enumerate_get_udev:
 * @list: (not used)
 *
 * Returns: Always returns #NULL.
 */
struct plug *
udev_enumerate_get_udev(struct devlist *list)
{
	return (NULL);
}


/**
 * udev_enumerate_scan_devices:
 * @list: (not used)
 *
 * Returns: Always returns 0.
 **/
int
udev_enumerate_scan_devices(struct devlist *list)
{
	return (0);
}


/**
 * udev_enumerate_scan_subsystems:
 * @list: plug device list
 *
 * Returns: 0
 **/
int
udev_enumerate_scan_subsystems(struct devlist *list)
{
	return (plug_devlist_scan_all(list));
}

/**
 * udev_enumerate_add_match_is_initialized:
 * @list: (not used)
 *
 * Returns: Always returns 0.
 **/
int
udev_enumerate_add_match_is_initialized(struct devlist *list)
{
	return (0);
}


/**
 *
 **/
int
udev_enumerate_add_match_property(struct devlist *list, const char *property,
    const char *value)
{
	warnx("function \"%s\" not supported.", __func__);
	return (-1);
}


/**
 *
 **/
int
udev_enumerate_add_match_sysattr(struct devlist *list, const char *attr,
    const char *value)
{
	warnx("function \"%s\" not supported.", __func__);
	return (-1);
}


/**
 *
 **/
int
udev_enumerate_add_nomatch_sysattr(struct devlist *list, const char *attr,
    const char *value)
{
	warnx("function \"%s\" not supported.", __func__);
	return (-1);
}


/**
 *
 **/
int
udev_enumerate_add_match_sysname(struct devlist *list, const char *name)
{
	warnx("function \"%s\" not supported.", __func__);
	return (-1);
}


/**
 *
 **/
int
udev_enumerate_add_match_tag(struct devlist *list, const char *tag)
{
	warnx("function \"%s\" not supported.", __func__);
	return (-1);
}


/**
 *
 **/
int
udev_enumerate_add_syspath(struct devlist *list, const char *path)
{
	warnx("function \"%s\" not supported.", __func__);
	return (-1);
}


/**
 * udev_enumerate_get_list_entry:
 * @devlist: plug device list
 *
 * Returns: the first property of a list of device paths.
 */
struct property *
udev_enumerate_get_list_entry(struct devlist *list)
{
	return (plug_devlist_get_devpaths(list));
}


/**
 * udev_enumerate_add_match_subsystem:
 * @list: plug device list
 * @name: subsystem name to scan
 *
 * Returns: 0 on success, a negative error value otherwise.
 */
int
udev_enumerate_add_match_subsystem(struct devlist *list, const char *name)
{
	return (plug_devlist_scan(list, name));
}


/**
 * udev_enumerate_add_nomatch_subsystem:
 * @list: plug device list
 * @name: subsystem name not to scan
 *
 * Returns: 0.
 */
int
udev_enumerate_add_nomatch_subsystem(struct devlist *list, const char *name)
{
	return (plug_devlist_scan_except(list, name));
}
