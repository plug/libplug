/*
 * Copyright (c) 2011 Martin Pieuchot
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/queue.h>
#include <sys/stat.h>
#include <sys/videoio.h>
#include <dev/usb/usb.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "plug.h"

#define USB_DEV		"usb"
#define USB_MAX_NODES	8

#define VIDEO_DEV	"video"
#define VIDEO_MAX_NODES	4

const char *plug_subsystems[SUBSYS_ALL] = {
	"block",
	"mem",
	"net",
	"tty",
	"usb",
	"video4linux",
};

struct device *
plug_usb_read_node(int fd, int addr)
{
	struct device		*dev;
	struct usb_device_info	 di;
	int 			 i;
	char			 devname[16], *name = NULL;

	di.udi_addr = (u_int8_t) addr;
	if (ioctl(fd, USB_DEVICEINFO, &di))
		return (NULL);

	/* XXX We only take the first devname */
	for (i = 0; i < USB_MAX_DEVNAMES; i++) {
		if (di.udi_devnames[i][0]) {
			name = di.udi_devnames[i];
			break;
		}
	}

	if (name == NULL)
		return (NULL);

	if (strncmp(name, "ugen", 4) == 0)
		snprintf(devname, sizeof(devname), "%s.00", name); 
	else
		strlcpy(devname, name, sizeof(devname));

	dev = plug_device_new(devname, plug_subsystems[SUBSYS_USB]);

	return (dev);
}

void
plug_usb_set_default_properties(struct device *dev)
{
	struct usb_device_info	di;
	int 			fd;
	char			nb[8];

	if ((fd = open(dev->devnode, O_RDONLY)) < 0)
		return;

	/* XXX Works only with ugen */
	if (ioctl(fd, USB_GET_DEVICEINFO, &di)) {
		warn("ioctl");
		close(fd);
		return;
	}
	close(fd);

	snprintf(nb, sizeof(nb), "%d", di.udi_bus);
	plug_set_property(dev, "BUSNUM", nb);

	snprintf(nb, sizeof(nb), "%d", di.udi_addr);
	plug_set_property(dev, "DEVNUM", nb);

	snprintf(nb, sizeof(nb), "%d", di.udi_vendorNo);
	plug_set_property(dev, "ID_VENDOR", di.udi_vendor);
	plug_set_property(dev, "ID_VENDOR_ID", nb);

	snprintf(nb, sizeof(nb), "%d", di.udi_productNo);
	plug_set_property(dev, "ID_MODEL", di.udi_product);
	plug_set_property(dev, "ID_MODEL_ID", nb);

	plug_set_property(dev, "ID_REVISION", di.udi_release);
	if (strlen(di.udi_serial))
		plug_set_property(dev, "ID_SERIAL_SHORT", di.udi_serial);

	/* XXX */
	plug_set_property(dev, "ID_GPHOTO2", "1");
}

int
plug_usb_scan(struct devlist *list)
{
	struct device	*dev;
	char		 node[16];
	int		 addr, fd, i;

	for (i = 0; i < USB_MAX_NODES; i++) {
		snprintf(node, sizeof(node), "%s/%s%d", DEVPATH, USB_DEV, i);

		fd = open(node, O_RDONLY);
		if (fd >= 0) {
			for (addr = 1; addr < USB_MAX_DEVICES; addr++) {
				dev = plug_usb_read_node(fd, addr);
				if (dev)
					TAILQ_INSERT_TAIL(list, dev, list);
			}
			close(fd);
		} else {
			if (errno == ENOENT || errno == ENXIO)
				continue;
			warn("%s", node);
		}
	}

	return (0);
}

void
plug_block_set_default_properties(struct device *dev)
{
	int		len;
	char		c;

	len = strlen(dev->pd_xname);
	c = dev->pd_xname[len - 1];

	if (c >= '0' && c <= '9')
		plug_set_property(dev, "DEVTYPE", "disk");
	else
		plug_set_property(dev, "DEVTYPE", "partition");

	if (c == 'a' || (c >= 'd' && c <= 'p'))
		plug_set_property(dev, "ID_FS_USAGE", "filesystem");
}

int
plug_video_scan(struct devlist *list)
{
	struct device		*dev;
	char			 node[16], name[16];
	struct v4l2_capability	 cap;
	int			 fd, i;

	for (i = 0; i < VIDEO_MAX_NODES; i++) {
		snprintf(node, sizeof(node), "%s/%s%d", DEVPATH, VIDEO_DEV, i);

		fd = open(node, O_RDONLY);
		if (fd >= 0) {
			if (ioctl(fd, VIDIOC_QUERYCAP, &cap))
				continue;

			snprintf(name, sizeof(name), "%s%d", VIDEO_DEV, i);
			dev = plug_device_new(name,
					      plug_subsystems[SUBSYS_VIDEO]);
			if (dev)
				TAILQ_INSERT_TAIL(list, dev, list);
			close(fd);
		} else {
			if (errno == ENOENT || errno == ENXIO)
				continue;
			warn("%s", node);
		}
	}

	return (0);
}

void
plug_video_set_default_properties(struct device *dev)
{
	struct v4l2_capability	 cap;
	int			 fd, len;
	char			 caps[64], *s;

	if ((fd = open(dev->devnode, O_RDONLY)) < 0)
		return;

	if (ioctl(fd, VIDIOC_QUERYCAP, &cap)) {
		warn("ioctl");
		close(fd);
		return;
	}
	close(fd);

	plug_set_property(dev, "ID_V4L_VERSION", "2");
	plug_set_property(dev, "ID_V4L_PRODUCT", cap.card);

	s = caps;
	*s = ':';
	len = 1;
	s++;

	if (cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) {
		len += snprintf(s, sizeof(caps) - len, "capture:");
		s = &caps[len];
	}
	if (cap.capabilities & V4L2_CAP_VIDEO_OUTPUT) {
		len += snprintf(s, sizeof(caps) - len, "video_ouput:");
		s = &caps[len];
	}
	if (cap.capabilities & V4L2_CAP_VIDEO_OVERLAY) {
		len += snprintf(s, sizeof(caps) - len, "video_overlay:");
		s = &caps[len];
	}
	if (cap.capabilities & V4L2_CAP_AUDIO) {
		len += snprintf(s, sizeof(caps) - len, "audio:");
		s = &caps[len];
	}
	if (cap.capabilities & V4L2_CAP_TUNER) {
		len += snprintf(s, sizeof(caps) - len, "tuner:");
		s = &caps[len];
	}
	if (cap.capabilities & V4L2_CAP_RADIO) {
		len += snprintf(s, sizeof(caps) - len, "radio:");
		s = &caps[len];
	}
	plug_set_property(dev, "ID_V4L_CAPABILITIES", caps);
}
